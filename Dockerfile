FROM rifqiakrm/grpc-go-base-builder:1.0.3-alpine AS base

# define timezone
ENV TZ Asia/Jakarta

# define work directory
WORKDIR /app

# copy the sourcecode
COPY . .

# generate protocol buffers
RUN make generate

# build beedoor exec
RUN cd /app && go mod vendor && make build

FROM alpine:3.16

WORKDIR /app

COPY --from=base /app/bin/server .

# EXPOSE 8080 is the port that the REST API will be exposed on
EXPOSE 50051
# EXPOSE 8080 is the port that the GRPC will be exposed on. But if deployed in cloud run just use the 8080 port

CMD [ "./server" ]